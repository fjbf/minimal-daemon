#!/usr/bin/env python3
# Paquetes necesarios (Debian 10):
#   python3-daemon

import datetime
import time
import daemon
import signal
import daemon.pidfile

import logging
import logging.handlers
import syslog

myname = "mydaemon"
pidfile = '/var/run/{}.pid'.format(myname)

syslog.openlog(myname)

def shutdown(signum, frame):
    syslog.syslog(syslog.LOG_INFO, "Saliendo...")
    sys.exit(0)
def main_program():
    while True:
        syslog.syslog(syslog.LOG_INFO, "{}".format(datetime.datetime.now()))
        time.sleep(1)

with daemon.DaemonContext(  chroot_directory=None,
                            working_directory='/tmp',
                            signal_map={
                            signal.SIGTERM: shutdown,
                            signal.SIGTSTP: shutdown
                            },
                            pidfile=daemon.pidfile.PIDLockFile(pidfile)): main_program()
